﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace minesweeper.Tests
{
    [TestClass()]
    public class BoxTests
    {
        [TestMethod()]
        public void GetPositionTest()
        {
            Tuple<int, int> toCompare = new Tuple<int, int>(0, 0);
            IBox box = new BoxImpl(toCompare);
            Assert.AreEqual(toCompare, box.GetPosition());
        }

        [TestMethod()]
        public void HitTest()
        {
            IBox box = new BoxImpl(new Tuple<int, int>(0, 0));
            Assert.IsFalse(box.IsClicked());
            box.Hit();
            Assert.IsTrue(box.IsClicked());
        }

        [TestMethod()]
        public void FlagTest()
        {
            IBox box = new BoxImpl(new Tuple<int, int>(0, 0));
            Assert.IsFalse(box.IsFlagged());
            box.SetFlag();
            Assert.IsTrue(box.IsFlagged());
        }

        [TestMethod()]
        public void ContainsBombTest()
        {
            IBox boxNoBomb = new BoxImpl(new Tuple<int, int>(0, 0), false);
            Assert.IsFalse(boxNoBomb.ContainsBomb());
            IBox boxWithBomb = new BoxImpl(new Tuple<int, int>(0, 0), true);
            Assert.IsTrue(boxWithBomb.ContainsBomb());
        }

        [TestMethod()]
        public void BombNearTest()
        {
            const int initBombNear = 0;
            const int endBombNear = 2;
            IBox box = new BoxImpl(new Tuple<int, int>(0, 0));
            Assert.AreEqual(box.GetBombNear(), initBombNear);
            box.SetBombNear(endBombNear);
            Assert.AreEqual(box.GetBombNear(), endBombNear);
        }
    }
}