﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace minesweeper.Tests
{
    [TestClass()]
    public class BoardTests
    {
        private readonly int width = 5;
        private readonly int height = 5;
        private IBoard board;

        [TestInitialize]
        public void TestInitialize()
        {
            IBoardBuilder boardBuilder = new BoardBuilderImpl();
            boardBuilder.WithHeight(height).WithWidth(width);
            for(int i=0; i < this.width; i++)
            {
                for (int j = 0; j < this.height; j++)
                {
                    boardBuilder.AddBox(new BoxImpl(new Tuple<int, int>(i, j), false));
                }
            }
            this.board = boardBuilder.Build();
        }

        [TestMethod()]
        public void GetWidthTest()
        {
            Assert.AreEqual(this.width, this.board.GetWidth());
        }

        [TestMethod()]
        public void GetHeightTest()
        {
            Assert.AreEqual(this.height, this.board.GetHeight());
        }

        [TestMethod()]
        public void GetBoxTest()
        {
            for (int i = 0; i < this.width; i++)
            {
                for (int j = 0; j < this.height; j++)
                {
                    Tuple<int, int> coord = new Tuple<int, int>(i, j);
                    IBox box1 = new BoxImpl(coord);
                    IBox box2 = this.board.GetBox(coord);
                    Assert.AreEqual(box1, box2);
                }
            }
        }

        [TestMethod()]
        public void GetNearBoxTest()
        {
            IBox box = new BoxImpl(new Tuple<int, int>(0, 0));
            
            IBox nearBox1 = new BoxImpl(new Tuple<int, int>(1, 1));
            IBox nearBox2 = new BoxImpl(new Tuple<int, int>(1, 0));
            IBox nearBox3 = new BoxImpl(new Tuple<int, int>(0, 1));

            IBox notNearBox1 = new BoxImpl(new Tuple<int, int>(2, 0));
            IBox notNearBox2 = new BoxImpl(new Tuple<int, int>(0, 2));
            IBox notNearBox3 = new BoxImpl(new Tuple<int, int>(2, 2));            

            Assert.IsTrue(this.board.GetNearBox(box).Contains(nearBox1));
            Assert.IsTrue(this.board.GetNearBox(box).Contains(nearBox2));
            Assert.IsTrue(this.board.GetNearBox(box).Contains(nearBox3));
            
            Assert.IsFalse(this.board.GetNearBox(box).Contains(notNearBox1));
            Assert.IsFalse(this.board.GetNearBox(box).Contains(notNearBox2));
            Assert.IsFalse(this.board.GetNearBox(box).Contains(notNearBox3));
        }
    }
}