﻿using System;
using System.Collections.Generic;
using System.Text;

namespace minesweeper
{
    public interface IBoardBuilder
    {
        IBoardBuilder WithWidth(int w);

        IBoardBuilder WithHeight(int h);

        IBoardBuilder AddBox(IBox box);

        IBoardBuilder AddBoxSet(HashSet<IBox> boxSet);

        IBoard Build();
    }
}
