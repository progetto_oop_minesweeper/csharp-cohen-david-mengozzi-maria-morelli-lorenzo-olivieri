﻿using System;
using System.Collections.Generic;

namespace minesweeper
{
    public class BoardBuilderImpl : IBoardBuilder
    {
        private int width;
        private int height;
        private readonly HashSet<IBox> boxSet = new HashSet<IBox>();

        public IBoardBuilder AddBox(IBox box)
        {
            this.boxSet.Add(box);
            return this;
        }

        public IBoardBuilder AddBoxSet(HashSet<IBox> boxSet)
        {
            this.boxSet.UnionWith(boxSet);
            return this;
        }

        public IBoardBuilder WithHeight(int h)
        {
            this.height = h;
            return this;
        }

        public IBoardBuilder WithWidth(int w)
        {
            this.width = w;
            return this;
        }

        public IBoard Build()
        {
            foreach (IBox box in this.boxSet)
            {
                box.SetBombNear(this.GetBombNear(box));
            }
            return new BoardImpl(this.width, this.height, this.boxSet);
        }

        private int GetBombNear(IBox selectedBox)
        {
            Tuple<int, int> selectedBoxPos = selectedBox.GetPosition();
            int count = 0;
            foreach (IBox box in this.boxSet)
            {
                if (BoardImpl.IsNear(selectedBoxPos, box.GetPosition()) && box.ContainsBomb())
                {
                    count++;
                }
            }

            return count;
        }
    }
}
