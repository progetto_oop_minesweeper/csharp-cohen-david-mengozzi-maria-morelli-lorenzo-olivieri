﻿using System;

namespace minesweeper
{
    public interface IBox
    {
        void SetFlag();

        void Hit();

        Tuple<int, int> GetPosition();

        bool ContainsBomb();

        bool IsClicked();

        bool IsFlagged();

        void SetBombNear(int bombNear);

        int GetBombNear();
    }
}
