﻿using System;
using System.Collections.Generic;

namespace minesweeper
{
    public interface IBoard
    {
        int GetWidth();

        int GetHeight();

        IBox GetBox(Tuple<int, int> coord);

        HashSet<IBox> GetNearBox(IBox selectedBox);

        int Size();
    }
}
