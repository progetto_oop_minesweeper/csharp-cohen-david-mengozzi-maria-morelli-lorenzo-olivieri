﻿using System;
using System.Collections.Generic;

namespace minesweeper
{
    public class BoxImpl : IBox
    {
        private readonly Tuple<int, int> coord;
        private readonly bool withBomb;
        private bool flag;
        private bool clicked;
        private int bombNear;

        public BoxImpl(Tuple<int, int> coord)
        {
            this.coord = coord;
            this.withBomb = false;
            this.flag = false;
            this.clicked = false;
            this.bombNear = 0;
        }

        public BoxImpl(Tuple<int, int> coord, bool withBomb)
        {
            this.coord = coord;
            this.withBomb = withBomb;
            this.flag = false;
            this.clicked = false;
            this.bombNear = 0;
        }

        public bool ContainsBomb()
        {
            return this.withBomb;
        }

        public int GetBombNear()
        {
            return this.bombNear;
        }

        public Tuple<int, int> GetPosition()
        {
            return this.coord;
        }

        public void Hit()
        {
            if (!this.IsFlagged())
            {
                this.clicked = true;
            }
        }

        public bool IsClicked()
        {
            return this.clicked;
        }

        public bool IsFlagged()
        {
            return this.flag;
        }

        public void SetBombNear(int bombNear)
        {
            this.bombNear = bombNear;
        }

        public void SetFlag()
        {
            if (!this.IsClicked())
            {
                this.flag = !this.IsFlagged();
            }
        }

        public override bool Equals(object obj)
        {
            return obj is BoxImpl impl &&
                   EqualityComparer<Tuple<int, int>>.Default.Equals(this.coord, impl.coord);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(coord);
        }

        public override string ToString()
        {
            if (this.IsClicked())
            {
                return this.bombNear.ToString();
            }
            else if (this.IsFlagged())
            {
                return "F";
            }
            else if (this.ContainsBomb())
            {
                return "B";
            }
            else
            {
                return "N";
            }
        }
    }
}
