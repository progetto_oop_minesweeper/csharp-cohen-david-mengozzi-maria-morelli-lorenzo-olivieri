﻿using System;
using System.Collections.Generic;

namespace minesweeper
{
    public class BoardImpl : IBoard
    {
        private const int NEAR_DISTANCE = 1;

        private readonly int width;
        private readonly int height;
        private readonly HashSet<IBox> boxSet;

        public BoardImpl(int width, int height, HashSet<IBox> boxSet)
        {
            this.width = width;
            this.height = height;
            this.boxSet = boxSet;
        }

        public int GetWidth()
        {
            return this.width;
        }

        public int GetHeight()
        {
            return this.height;
        }

        public IBox GetBox(Tuple<int, int> coord)
        {
            foreach (IBox box in boxSet)
            {
                if (box.GetPosition().Equals(coord))
                {
                    return box;
                }
            }
            throw new Exception("Box not found");
        }

        public HashSet<IBox> GetNearBox(IBox selectedBox)
        {
            HashSet<IBox> set = new HashSet<IBox>();
            Tuple<int, int> selectedBoxPos = selectedBox.GetPosition();
            foreach (IBox box in this.boxSet)
            {
                if (IsNear(selectedBoxPos, box.GetPosition()))
                {
                    set.Add(box);
                }
            }

            return set;
        }

        public int Size()
        {
            return this.boxSet.Count;
        }

        public static bool IsNear(Tuple<int, int> pos1, Tuple<int, int> pos2)
        {
            return !pos1.Equals(pos2) && Math.Abs(pos1.Item1 - pos2.Item1) <= NEAR_DISTANCE
                    && Math.Abs(pos1.Item2 - pos2.Item2) <= NEAR_DISTANCE;
        }
    }
}
