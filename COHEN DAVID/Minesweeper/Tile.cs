﻿using System;
using System.Windows.Forms;

namespace Mineswepeers
{
    public class Tile : Button, ITile
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int Value { get; set; }
        public bool Flag { get; set; }

        public Tile(int x, int y, int size)
        {
            X = x;
            Y = y;
            Height = size;
            Width = size;
            TabStop = false;

            Click += (object sender, EventArgs e) =>
            {


            };
        }

        public void SetFlag()
        {
            Flag = !Flag;
            if(Flag)
            {
                Text = "F";
            }
            else
            {
                Text = "";
            }
        }

    }
}
