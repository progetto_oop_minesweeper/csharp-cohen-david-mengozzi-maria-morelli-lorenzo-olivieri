﻿using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Mineswepeers
{
    class TileBuilder : ITileBuilder
    {
        private int height;
        private int width;
        private Panel grid;
        private readonly static int SIZE = 55;


        public object Controls { get; private set; }

        public ITileBuilder WithHeight(int height)
        {
            this.height = height;
            return this;
        }

        public ITileBuilder WithWidth(int width)
        {
            this.width = width;
            return this;
        }

        public ITileBuilder WithGrid(Panel grid)
        {
            this.grid = grid;
            return this;
        }

        public List<Tile> Build()
        {
                var tileList = new List<Tile>();
                for (int r = 0; r < width; r++)
                {
                    for (int c = 0; c < height; c++)
                    {
                        tileList.Add(CreateTile(r, c));
                    }
                }
                return tileList;

        }

        private Tile CreateTile(int r, int c)
        {
            Tile tile = new Tile(r, c, SIZE)
            {
                Location = new Point(r * SIZE , c * SIZE)
                
            };
            return tile;
        }
    }
}
