﻿namespace Mineswepeers
{
    interface ITile
    {
        /// <returns>return the x value</returns>
        int X { get; set; }

        /// <returns>return the y value</returns>
        int Y { get; set; }

        /// <returns>Return the valueo of <seealso cref="ITile"></returns>
        int Value { get; set; }

        /// <returns>Return the value of Flag</returns>
        bool Flag { get; set; }

        /// <summary>Set the Value of the <seealso cref="ITile"></returns> </summary>
        /// <param name="value">the Value of the <seealso cref="ITile"></param>
        void SetFlag();
    }
}
