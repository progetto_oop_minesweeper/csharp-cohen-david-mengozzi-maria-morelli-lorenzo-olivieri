﻿namespace Mineswepeers
{
    partial class SinglePlayerController
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblMines = new System.Windows.Forms.Label();
            this.Grid = new System.Windows.Forms.Panel();
            this.lblFlags = new System.Windows.Forms.Label();
            this.Tabella = new System.Windows.Forms.TableLayoutPanel();
            this.lblName = new System.Windows.Forms.Label();
            this.lblTimer = new System.Windows.Forms.Label();
            this.Tabella.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblMines
            // 
            this.lblMines.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.lblMines.AutoSize = true;
            this.lblMines.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMines.Location = new System.Drawing.Point(829, 0);
            this.lblMines.Name = "lblMines";
            this.lblMines.Size = new System.Drawing.Size(198, 108);
            this.lblMines.TabIndex = 3;
            this.lblMines.Text = "lblMines";
            this.lblMines.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Grid
            // 
            this.Grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Grid.Location = new System.Drawing.Point(153, 111);
            this.Grid.Name = "Grid";
            this.Grid.Size = new System.Drawing.Size(669, 612);
            this.Grid.TabIndex = 0;
            // 
            // lblFlags
            // 
            this.lblFlags.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.lblFlags.AutoSize = true;
            this.lblFlags.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFlags.Location = new System.Drawing.Point(12, 0);
            this.lblFlags.Name = "lblFlags";
            this.lblFlags.Size = new System.Drawing.Size(126, 108);
            this.lblFlags.TabIndex = 2;
            this.lblFlags.Text = "lblFlags";
            this.lblFlags.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Tabella
            // 
            this.Tabella.ColumnCount = 3;
            this.Tabella.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.58741F));
            this.Tabella.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 65.41872F));
            this.Tabella.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.Tabella.Controls.Add(this.lblMines, 2, 0);
            this.Tabella.Controls.Add(this.lblFlags, 0, 0);
            this.Tabella.Controls.Add(this.lblName, 1, 2);
            this.Tabella.Controls.Add(this.lblTimer, 1, 0);
            this.Tabella.Controls.Add(this.Grid, 1, 1);
            this.Tabella.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Tabella.Location = new System.Drawing.Point(0, 0);
            this.Tabella.Name = "Tabella";
            this.Tabella.RowCount = 3;
            this.Tabella.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.56591F));
            this.Tabella.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 71.79262F));
            this.Tabella.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15.64148F));
            this.Tabella.Size = new System.Drawing.Size(1032, 861);
            this.Tabella.TabIndex = 0;
            // 
            // lblName
            // 
            this.lblName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblName.AutoSize = true;
            this.lblName.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.Location = new System.Drawing.Point(436, 726);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(102, 61);
            this.lblName.TabIndex = 4;
            this.lblName.Text = "m  ";
            this.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTimer
            // 
            this.lblTimer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.lblTimer.AutoSize = true;
            this.lblTimer.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTimer.Location = new System.Drawing.Point(377, 0);
            this.lblTimer.Name = "lblTimer";
            this.lblTimer.Size = new System.Drawing.Size(220, 108);
            this.lblTimer.TabIndex = 1;
            this.lblTimer.Text = "lblTimer";
            this.lblTimer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // SinglePlayerController
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1032, 861);
            this.Controls.Add(this.Tabella);
            this.Name = "SinglePlayerController";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tabella.ResumeLayout(false);
            this.Tabella.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lblMines;
        private System.Windows.Forms.Panel Grid;
        private System.Windows.Forms.Label lblFlags;
        private System.Windows.Forms.TableLayoutPanel Tabella;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblTimer;
    }
}

