﻿using System.Collections.Generic;
using System.Windows.Forms;

namespace Mineswepeers
{
    interface ITileBuilder
    {
        /// <summary>Set the <seealso cref="ITileBuilder"> Width</summary>
        /// <param name="width">Width of <seealso cref="ITileBuilder"></param>
        /// <returns>Return <seealso cref="ITileBuilder"></returns>
        ITileBuilder WithWidth(int width);

        /// <summary>Set the <seealso cref="ITileBuilder"> height</summary>
        /// <param name="height">Height of <seealso cref="ITileBuilder"></param>
        /// <returns>Return <seealso cref="ITileBuilder"></returns>
        ITileBuilder WithHeight(int height);

        /// <summary>Set the <seealso cref="ITileBuilder"> Panel</summary>
        /// <param name="grid">Width of <seealso cref="ITileBuilder"></param>
        /// <returns>Return <seealso cref="ITileBuilder"></returns>
        ITileBuilder WithGrid(Panel grid);

        /// <summary>Build the <seealso cref="ITile"></returns> </summary>
        /// <returns>Builded <seealso cref="ITileBuilder"></returns>
        List<Tile> Build();
    }
}
