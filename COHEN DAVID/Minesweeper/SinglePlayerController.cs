﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Mineswepeers
{
    public partial class SinglePlayerController : Form
    {
        private readonly int height = 10;
        private readonly int width = 12;
        private readonly int mines = 20;
        private bool firstclick;
        private int ccflags;
        private IList tileList = new List<Tile>();

        public SinglePlayerController()
        {
            InitializeComponent();
            InizializeGame();
        }

        /// <summary>Initialize the elements of the <seealso cref="SinglePlayerController"></summary>
        private void InizializeGame()
        {
            lblFlags.Text= "FLAGS:" + mines;
            lblName.Text = "NONE";
            lblMines.Text = "MINES:" + mines;
            ccflags = mines;
            firstclick = true;
            ITileBuilder tb = new TileBuilder();
            tb.WithHeight(height);
            tb.WithWidth(width);
            tb.WithGrid(Grid);
            tileList = tb.Build();
            foreach (Tile tile in tileList)
            {
                Grid.Controls.Add(tile);
            }
            SetClickHandler();
        }

        /// <summary>Set handler of <seealso cref="ITile"></summary>
        private void SetClickHandler()
        {
            foreach (Tile tile in tileList)
            {
                tile.MouseUp += (e, args) =>
                {
                    if (args.Button == MouseButtons.Right)
                    {
                        RightClickHandler(tile);
                    }
                    else if (args.Button == MouseButtons.Left)
                    {
                        LeftClickHandler(tile);
                    }
                };
            }
        }

        /// <summary>Set left click handler of <seealso cref="ITile"></summary>
        /// <param name="tile"><seealso cref="ITile"></param>
        private void LeftClickHandler(Tile tile)
        {
            if(firstclick)
            {
                firstclick = false;
                lblTimer.Text = "00:00";
            }
            Console.WriteLine("X: " + tile.X.ToString() + " Y: " + tile.Y.ToString() + " Value: " + tile.Value.ToString());
            if (!tile.Flag)
            {
                tile.Enabled = false;
            }
        }

        /// <summary>Set right click handler of <seealso cref="ITile"></summary>
        /// <param name="tile"><seealso cref="ITile"></param>
        private void RightClickHandler(Tile tile)
        {
            if(!tile.Flag)
            {
                ccflags--;
            } else
            {
                ccflags++;
            }
            tile.SetFlag();
            if(ccflags >= 0 && ccflags < 10)
            {
                lblFlags.Text = "FLAGS:0" + ccflags;
            } else
            {
                lblFlags.Text = "FLAGS:" + ccflags;
            }
        }
    }
}
