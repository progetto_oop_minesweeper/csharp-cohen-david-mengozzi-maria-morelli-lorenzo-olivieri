﻿namespace CSharpMinesweeper
{
    public interface ITimer
    {
        long GetValue();

        void Start();
        
        void Stop();

        bool IsRunning();

        int GetLimit();
    }
}
