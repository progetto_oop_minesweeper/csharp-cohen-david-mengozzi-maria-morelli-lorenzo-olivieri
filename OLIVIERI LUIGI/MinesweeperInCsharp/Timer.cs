﻿using System;

namespace CSharpMinesweeper
{
    class Timer : ITimer
    {
        private readonly DateTime reference = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        private readonly Verse verse;
        private readonly int limit;

        private long initialTime;
        private long startTime;
        private bool stop;

        public Timer(long initialTime, Verse verse)
        {
            this.initialTime = initialTime;
            this.verse = verse;
            limit = verse.GetLimit() * 1_000;
            stop = true;
        }

        public long GetValue()
        {
            if (IsRunning())
            {
                if (LimitNotReached())
                {
                    initialTime += (CurrentMillisec() - startTime) * verse.GetVerseIncrementValue();
                    startTime = CurrentMillisec();
                }
                else
                {
                    Stop();
                    initialTime = limit;
                }
            }
            return initialTime;
        }

        public bool IsRunning()
        {
            return !stop;
        }

        public void Start()
        {
            stop = false;
            startTime = CurrentMillisec();
        }

        public void Stop()
        {
            stop = true;
        }

        public int GetLimit()
        {
            return limit;
        }

        private bool LimitNotReached()
        {
            return (initialTime - limit) * verse.GetVerseIncrementValue() < 0;
        }

        private long CurrentMillisec()
        {
            TimeSpan currentMillis = DateTime.UtcNow - reference;
            return (long)currentMillis.TotalMilliseconds;
        }
    }
}
