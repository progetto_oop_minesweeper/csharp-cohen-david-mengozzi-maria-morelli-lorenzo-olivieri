﻿using CSharpMinesweeper;
using System;
using System.Windows.Forms;

namespace MinesweeperInCsharp
{
    public partial class Timers : Form
    {
        private readonly ITimerFactory f;

        private ITimerView view1;
        private ITimerView view2;

        IDoubleTimer doubleTimer;
        ITimer timer;

        private Mode mode;

        public Timers()
        {
            InitializeComponent();
            f = new TimerFactory();
            play.Enabled = false;
            switchTurn.Enabled = false;
        }

        private void Standard_Click(object sender, EventArgs e)
        {
            mode = Mode.STD;

            timer = f.CreateTimerForStandardMode();
            view1 = new TimerView(timer, timer1Label);
            play.Enabled = true;

            view1.StartDisplaying();

            SetChoiceButtons(false);
        }

        private void BeatTheTimer_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(textInitialTime.Text) && TextIsNumber(textInitialTime.Text))
            {
                mode = Mode.BTT;

                timer = f.CreateTimerForBeatTheTimerMode(long.Parse(textInitialTime.Text));
                view1 = new TimerView(timer, timer1Label);
                play.Enabled = true;

                view1.StartDisplaying();

                SetChoiceButtons(false);
            }
        }

        private bool TextIsNumber(string text)
        {
            foreach (char c in text)
            {
                if (!char.IsDigit(c))
                {
                    return false;
                }
            }
            return true;
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            mode = Mode.OVO;

            doubleTimer = f.CreateTimersFor1vs1Mode();
            view1 = new TimerView(doubleTimer.GetPlayer1Timer(), timer1Label);
            view2 = new TimerView(doubleTimer.GetPlayer2Timer(), timer2Label);

            view1.StartDisplaying();
            view2.StartDisplaying();

            play.Enabled = true;
            switchTurn.Enabled = true;

            SetChoiceButtons(false);
        }

        private void Reset_Click(object sender, EventArgs e)
        {
            play.Enabled = false;
            switchTurn.Enabled = false;

            SetChoiceButtons(true);

            view1.StopDisplaying();
            timer1Label.Text = "";

            if (mode == Mode.OVO)
            {
                view2.StopDisplaying();
                timer2Label.Text = "";
            }
            play.Text = "Play";
        }

        private void Play_Click(object sender, EventArgs e)
        {
            if(mode == Mode.OVO)
            {
                StartStop(doubleTimer);
            }
            else
            {
                StartStop(timer);
            }
        }

        private void StartStop(ITimer timer)
        {
            if (timer.IsRunning())
            {
                timer.Stop();
                play.Text = "Play";
            }
            else
            {
                timer.Start();
                play.Text = "Pause";
            }
        }

        private void SwitchTurn_Click(object sender, EventArgs e)
        {
            doubleTimer.SwitchTurn();
        }

        private void SetChoiceButtons(bool b)
        {
            standard.Enabled = b;
            beatTheTimer.Enabled = b;
            oneVsOne.Enabled = b;
        }

        enum Mode
        {
            STD,
            OVO,
            BTT
        }
    }
}
