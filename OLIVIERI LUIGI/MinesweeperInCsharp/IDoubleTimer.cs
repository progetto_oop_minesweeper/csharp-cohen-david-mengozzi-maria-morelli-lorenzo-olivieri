﻿namespace CSharpMinesweeper
{
    public interface IDoubleTimer : IMultipleTimers
    {
        ITimer GetPlayer1Timer();

        ITimer GetPlayer2Timer();
    }
}
