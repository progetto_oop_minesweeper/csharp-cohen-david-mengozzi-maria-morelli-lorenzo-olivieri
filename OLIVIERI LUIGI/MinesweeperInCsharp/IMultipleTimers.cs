﻿namespace CSharpMinesweeper
{
    public interface IMultipleTimers : ITimer
    {
        void SwitchTurn();
    }
}
