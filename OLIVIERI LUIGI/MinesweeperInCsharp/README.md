# README
Qui sono riportate le classi principali del package timer tramutate in C#.

Per vedere il loro funzionamento è stata creata una piccola GUI che permette di scegliere 
il timer per una delle tre modalità all'interno di Minesweeper e vederlo in azione.

In particolare i tre Timer funzioneranno così:

* **Standard:** un Timer che cresce da 0.

* **Beat the Timer:** un Timer che scende dal valore inziale dato (_attraverso il numero affianco al bottone di scelta_).

* **1 vs 1:** due Timer che crescono da 0 uno alla volta quando è il loro turno.

Alcune classi sono state _leggermente_ modificate in quanto alcune funzionalità e librerie 
utilizate in Java non avevano una controparte identica in C#.

## Autore

Luigi Olivieri.

## Utilizzo

* Aviare l'applicazione.

* Selezionare una delle tre **Modalità** per la quale verrà utilizzato il Timer. 

* Premere **Play** per far partire il Timer selezionato.

* **Nella Modalità 1 vs 1** premere **Switch Turn** per simulare i turni dei giocatori.

* Premere **Reset** per utilizare un altro Timer. 