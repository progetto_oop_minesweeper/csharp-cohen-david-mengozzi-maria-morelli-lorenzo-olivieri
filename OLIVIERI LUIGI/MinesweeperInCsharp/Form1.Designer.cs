﻿using System;

namespace MinesweeperInCsharp
{
    partial class Timers
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.oneVsOne = new System.Windows.Forms.Button();
            this.beatTheTimer = new System.Windows.Forms.Button();
            this.standard = new System.Windows.Forms.Button();
            this.reset = new System.Windows.Forms.Button();
            this.timer1Label = new System.Windows.Forms.Label();
            this.play = new System.Windows.Forms.Button();
            this.switchTurn = new System.Windows.Forms.Button();
            this.textInitialTime = new System.Windows.Forms.TextBox();
            this.timer2Label = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // oneVsOne
            // 
            this.oneVsOne.Location = new System.Drawing.Point(38, 129);
            this.oneVsOne.Name = "oneVsOne";
            this.oneVsOne.Size = new System.Drawing.Size(119, 30);
            this.oneVsOne.TabIndex = 0;
            this.oneVsOne.Text = "1 vs 1";
            this.oneVsOne.UseVisualStyleBackColor = true;
            this.oneVsOne.Click += new System.EventHandler(this.Button1_Click);
            // 
            // beatTheTimer
            // 
            this.beatTheTimer.Location = new System.Drawing.Point(38, 91);
            this.beatTheTimer.Name = "beatTheTimer";
            this.beatTheTimer.Size = new System.Drawing.Size(119, 32);
            this.beatTheTimer.TabIndex = 1;
            this.beatTheTimer.Text = "Beat the Timer";
            this.beatTheTimer.UseVisualStyleBackColor = true;
            this.beatTheTimer.Click += new System.EventHandler(this.BeatTheTimer_Click);
            // 
            // standard
            // 
            this.standard.Location = new System.Drawing.Point(38, 52);
            this.standard.Name = "standard";
            this.standard.Size = new System.Drawing.Size(119, 33);
            this.standard.TabIndex = 2;
            this.standard.Text = "Standard";
            this.standard.UseVisualStyleBackColor = true;
            this.standard.Click += new System.EventHandler(this.Standard_Click);
            // 
            // reset
            // 
            this.reset.Location = new System.Drawing.Point(54, 186);
            this.reset.Name = "reset";
            this.reset.Size = new System.Drawing.Size(75, 33);
            this.reset.TabIndex = 3;
            this.reset.Text = "RESET";
            this.reset.UseVisualStyleBackColor = true;
            this.reset.Click += new System.EventHandler(this.Reset_Click);
            // 
            // timer1Label
            // 
            this.timer1Label.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.timer1Label.AutoSize = true;
            this.timer1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 25.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timer1Label.Location = new System.Drawing.Point(285, 33);
            this.timer1Label.MinimumSize = new System.Drawing.Size(100, 0);
            this.timer1Label.Name = "timer1Label";
            this.timer1Label.Size = new System.Drawing.Size(100, 51);
            this.timer1Label.TabIndex = 4;
            this.timer1Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // play
            // 
            this.play.Location = new System.Drawing.Point(294, 168);
            this.play.Name = "play";
            this.play.Size = new System.Drawing.Size(75, 27);
            this.play.TabIndex = 5;
            this.play.Text = "Play";
            this.play.UseVisualStyleBackColor = false;
            this.play.Click += new System.EventHandler(this.Play_Click);
            // 
            // switchTurn
            // 
            this.switchTurn.Location = new System.Drawing.Point(283, 214);
            this.switchTurn.Name = "switchTurn";
            this.switchTurn.Size = new System.Drawing.Size(98, 29);
            this.switchTurn.TabIndex = 6;
            this.switchTurn.Text = "Switch Turn";
            this.switchTurn.UseVisualStyleBackColor = true;
            this.switchTurn.Click += new System.EventHandler(this.SwitchTurn_Click);
            // 
            // textInitialTime
            // 
            this.textInitialTime.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textInitialTime.CausesValidation = false;
            this.textInitialTime.Location = new System.Drawing.Point(163, 99);
            this.textInitialTime.Multiline = true;
            this.textInitialTime.Name = "textInitialTime";
            this.textInitialTime.Size = new System.Drawing.Size(44, 17);
            this.textInitialTime.TabIndex = 7;
            this.textInitialTime.Text = "100";
            this.textInitialTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // timer2Label
            // 
            this.timer2Label.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.timer2Label.AutoSize = true;
            this.timer2Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 25.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timer2Label.Location = new System.Drawing.Point(285, 96);
            this.timer2Label.MinimumSize = new System.Drawing.Size(100, 0);
            this.timer2Label.Name = "timer2Label";
            this.timer2Label.Size = new System.Drawing.Size(100, 51);
            this.timer2Label.TabIndex = 8;
            this.timer2Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Timers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(450, 275);
            this.Controls.Add(this.timer2Label);
            this.Controls.Add(this.textInitialTime);
            this.Controls.Add(this.switchTurn);
            this.Controls.Add(this.play);
            this.Controls.Add(this.timer1Label);
            this.Controls.Add(this.reset);
            this.Controls.Add(this.standard);
            this.Controls.Add(this.beatTheTimer);
            this.Controls.Add(this.oneVsOne);
            this.Name = "Timers";
            this.Text = "Timers";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button oneVsOne;
        private System.Windows.Forms.Button beatTheTimer;
        private System.Windows.Forms.Button standard;
        private System.Windows.Forms.Button reset;
        private System.Windows.Forms.Label timer1Label;
        private System.Windows.Forms.Button play;
        private System.Windows.Forms.Button switchTurn;
        private System.Windows.Forms.TextBox textInitialTime;
        private System.Windows.Forms.Label timer2Label;
    }
}

