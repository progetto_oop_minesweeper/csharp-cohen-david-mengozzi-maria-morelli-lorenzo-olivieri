﻿namespace CSharpMinesweeper
{
    public enum Verse
    {
        UP = 1,
        DOWN = -1,
    }

    public static class VerseMethods
    {
        public static int GetLimit(this Verse verse) => verse.Equals(Verse.UP) ? 999 : 0;
        public static int GetVerseIncrementValue(this Verse verse) => (int)verse;
    }
}