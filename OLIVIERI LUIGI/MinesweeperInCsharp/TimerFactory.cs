﻿namespace CSharpMinesweeper
{
    public class TimerFactory : ITimerFactory
    {
        public ITimer CreateTimerForStandardMode()
        {
            return new Timer(0, Verse.UP);
        }

        public ITimer CreateTimerForBeatTheTimerMode(long amountOfTime)
        {
            return new Timer(amountOfTime * 1_000, Verse.DOWN);
        }

        public IDoubleTimer CreateTimersFor1vs1Mode()
        {
            return new DoubleTimer(CreateTimerForStandardMode(), CreateTimerForStandardMode());
        }
    }
}
