﻿using CSharpMinesweeper;
using System;
using System.Timers;
using System.Windows.Forms;

namespace MinesweeperInCsharp
{
    class TimerView : ITimerView
    {
        private const int UPDATE_RATE = 1_000;

        private readonly ITimer timer;
        private readonly Label label;

        private readonly System.Windows.Forms.Timer displayRefresher;

        public TimerView(ITimer timer, Label label)
        {
            this.label = label;
            this.timer = timer;
            displayRefresher = new System.Windows.Forms.Timer();
            displayRefresher.Interval = UPDATE_RATE / 4;
        }

        public void StartDisplaying()
        {
            displayRefresher.Enabled = true;

            displayRefresher.Tick += new EventHandler(UpdateView);

            displayRefresher.Start();
        }

        private void UpdateView(Object myObject, EventArgs myEventArgs)
        {
            long currentTime = timer.GetValue() / UPDATE_RATE;
            label.Text = currentTime.ToString();
            if (currentTime == timer.GetLimit())
            {
                StopDisplaying();
            }
        }

        public void StopDisplaying()
        {
            displayRefresher.Stop();
            displayRefresher.Enabled = false;
        }
    }
}
