﻿namespace CSharpMinesweeper
{
    internal class DoubleTimer : IDoubleTimer
    {
        private readonly ITimer player1Timer;
        private readonly ITimer player2Timer;

        public DoubleTimer(ITimer player1Timer, ITimer player2Timer)
        {
            this.player1Timer = player1Timer;
            this.player2Timer = player2Timer;
        }

        public long GetValue()
        {
            return RunningTimer().GetValue();
        }

        public bool IsRunning()
        {
            return RunningTimer().IsRunning();
        }

        public void Start()
        {
            player1Timer.Start();
        }

        public void Stop()
        {
            player1Timer.Stop();
            player2Timer.Stop();
        }
        public void SwitchTurn()
        {
            ITimer oldRunningTimer = RunningTimer();
            RunningTimer().Stop();

            if (oldRunningTimer.Equals(player1Timer))
            {
                player2Timer.Start();
            }
            else
            {
                player1Timer.Start();
            }
        }

        public ITimer GetPlayer1Timer()
        {
            return player1Timer;
        }

        public ITimer GetPlayer2Timer()
        {
            return player2Timer;
        }

        public int GetLimit()
        {
            return RunningTimer().GetLimit();
        }

        private ITimer RunningTimer()
        {
            if (player1Timer.IsRunning())
            {
                return player1Timer;
            }
            else
            {
                return player2Timer;
            }
        }
    }
}