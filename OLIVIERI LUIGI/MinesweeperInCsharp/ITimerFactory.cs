﻿namespace CSharpMinesweeper
{
    public interface ITimerFactory
    {
        ITimer CreateTimerForStandardMode();

        ITimer CreateTimerForBeatTheTimerMode(long amountOfTime);

        IDoubleTimer CreateTimersFor1vs1Mode();
    }
}
