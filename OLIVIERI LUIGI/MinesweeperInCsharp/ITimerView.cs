﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinesweeperInCsharp
{
    interface ITimerView
    {
        void StartDisplaying();

        void StopDisplaying();
    }
}
