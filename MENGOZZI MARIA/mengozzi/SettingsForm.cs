﻿using MengozziOOP19;
using System;

using System.Drawing;
using System.IO;
using System.Media;
using System.Windows.Forms;


namespace mengozziOop19
{
    public partial class SettingsForm : Form
    {
        private IReadWriteSettings rwSett;
        private string minePath;
        private string flagPath;
        private string songPath;
        public SettingsForm()
        {
            InitializeComponent();
            rwSett = new ReadWriteSettings();
            LoadPath();
            /**inizializzo i componenti secondo quanto salvato su file*/
            UploadMineSelector();
            UploadFlagSelector();
            UploadSongSelector();
            MineImg(rwSett.Mine);
            FlagsImg(rwSett.Flags);
            AddColor(rwSett.Color);
            SongSound(rwSett.Song);

        }

        private void LoadPath()
        {
            var homeDrive = Environment.GetEnvironmentVariable("HOMEDRIVE");
            if (!string.IsNullOrWhiteSpace(homeDrive))
            {

                var homePath = Environment.GetEnvironmentVariable("HOMEPATH");
                if (!string.IsNullOrWhiteSpace(homePath))
                {
                    var fullHomePath = homeDrive + Path.DirectorySeparatorChar + homePath;
                    string path = Path.Combine(fullHomePath, ".CSminesweeper");
                    flagPath = Path.Combine(path, "FlagImg");
                    minePath = Path.Combine(path, "MineImg");
                    songPath = Path.Combine(path, "Songs");
                }
                else
                {
                    throw new Exception("Environment variable error, there is no 'HOMEPATH'");
                }
            }
            else
            {
                throw new Exception("Environment variable error, there is no 'HOMEDRIVE'");
            }
        }

        private void AddColor(string color)
        {
            Color c = Color.FromName(color);
            if (!c.IsKnownColor)
            { 
                string hexCol = string.Concat("#", color);
                c = System.Drawing.ColorTranslator.FromHtml(hexCol);
            }
            title.ForeColor = c;
            selectColor.BackColor = c;
            btTest.BackColor = c;
            exit.BackColor = c;
        }

        private void SongSound(string song)
        {
            tsSongSelector.Text = song;
        }

        private void FlagsImg(string flags)
        {
            
            Image img = Image.FromFile(Path.Combine(flagPath, flags));
            tsFlagSelector.Text = flags;
            pbFlag.Image = img;

        }

        private void MineImg(string mine)
        {
            Image img = Image.FromFile(Path.Combine(minePath, mine));
            tsMineSelector.Text = mine;
            pbMine.Image = img;
        }

        private SoundPlayer simpleSound;
        private OpenFileDialog ofp = new OpenFileDialog();

        private void BtAddImgMine_Click(object sender, EventArgs e)
        {
            ofp.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.png)|*.jpg; *.jpeg; *.gif; *.png";
            if (ofp.ShowDialog() == DialogResult.OK)
            {
                Image img = Image.FromFile(ofp.FileName);
                pbMine.Image = img;
                img.Save(Path.Combine(minePath, ofp.SafeFileName));
                rwSett.Mine = ofp.SafeFileName;
                tsMineSelector.Text = rwSett.Mine;
                UploadMineSelector();
            }
        }

        private void MineImgSelector_Click(object sender, EventArgs e)
        {
            string name = sender .ToString();
            Image img = Image.FromFile(Path.Combine(minePath, name));
            tsMineSelector.Text = name;
            pbMine.Image = img;
            rwSett.Mine = name;

        }


    private void UploadMineSelector()
        {
            if (tsMineSelector.DropDownItems != null)
            {
                tsMineSelector.DropDownItems.Clear();
            }
            string[] fileEntries = Directory.GetFiles(minePath);
            for (int i = 0; i< fileEntries.Length ; i++)
            {
                string strFile = fileEntries[i];
                ToolStripItem item = new ToolStripMenuItem
                {
                    //Name that will apear on the menu
                    Text = Path.GetFileName(strFile),
                    Tag = strFile,
                    Name = Text

                };
                //On-Click event
                item.Click += new EventHandler(MineImgSelector_Click);
                //Add the submenu to the parent menu
                tsMineSelector.DropDownItems.Add(item);
            }
            
        }


        private void BtAddImgFlag_Click(object sender, EventArgs e)
        {
            ofp.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.png)|*.jpg; *.jpeg; *.gif; *.png";
            if (ofp.ShowDialog() == DialogResult.OK)
            {
                Image img = Image.FromFile(ofp.FileName);
                pbFlag.Image = img;
                img.Save(Path.Combine(flagPath, ofp.SafeFileName));
                rwSett.Flags = ofp.SafeFileName;
                tsFlagSelector.Text = rwSett.Flags;
                UploadFlagSelector();
            }
        }

        private void FlagImgSelector_Click(object sender, EventArgs e)
        {
            string name = sender.ToString();
            Image img = Image.FromFile(Path.Combine(flagPath, name));
            tsFlagSelector.Text = name;
            pbFlag.Image = img;
            rwSett.Flags = name;

        }


        private void UploadFlagSelector()
        {
            if (tsFlagSelector.DropDownItems != null)
            {
                tsFlagSelector.DropDownItems.Clear();
            }
            string[] fileEntries = Directory.GetFiles(flagPath);
            for (int i = 0; i < fileEntries.Length; i++)
            {
                string strFile = fileEntries[i];
                ToolStripItem item = new ToolStripMenuItem
                {
                    //Name that will apear on the menu
                    Text = Path.GetFileName(strFile),
                    Tag = strFile,
                    Name = Text

                };
                //On-Click event
                item.Click += new EventHandler(FlagImgSelector_Click);
                //Add the submenu to the parent menu
                tsFlagSelector.DropDownItems.Add(item);
            }

        }
        private void SelectColor_Click(object sender, EventArgs e)
        {
            ColorDialog colorDialog = new ColorDialog();
            if (colorDialog.ShowDialog() == DialogResult.OK)
            {
                Color cl = colorDialog.Color;
                selectColor.BackColor = cl;
                title.ForeColor = cl;
                rwSett.Color = cl.Name;
                btTest.BackColor = cl;
                exit.BackColor = cl;
            }
        }

        private void SongSelector_Click(object sender, EventArgs e)
        {
            string name = sender.ToString();
            simpleSound = new SoundPlayer(Path.Combine(songPath, name));
            simpleSound.Play();
            rwSett.Song = name;
            tsSongSelector.Text = rwSett.Song;

        }


        private void UploadSongSelector()
        {
            if (tsSongSelector.DropDownItems != null)
            {
                tsSongSelector.DropDownItems.Clear();
            }
            string[] fileEntries = Directory.GetFiles(songPath);
            for (int i = 0; i < fileEntries.Length; i++)
            {
                string strFile = fileEntries[i];
                ToolStripItem item = new ToolStripMenuItem
                {
                    //Name that will apear on the menu
                    Text = Path.GetFileName(strFile),
                    Tag = strFile,
                    Name = Text

                };
                //On-Click event
                item.Click += new EventHandler(SongSelector_Click);
                //Add the submenu to the parent menu
                tsSongSelector.DropDownItems.Add(item);
            }

        }

        private void StopButton_Click(object sender, EventArgs e)
        {
            if (simpleSound != null)
            {
                simpleSound.Stop();
            }
        }

        private void ExitButton_Click(object sender, EventArgs e)
        {
            // Initializes the variables to pass to the MessageBox.Show method.
            string message = "are you sure you want to exist";
            const string caption = "Form Closing";
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result;

            // Displays the MessageBox.
            result = MessageBox.Show(message, caption, buttons);
            if (result == DialogResult.Yes)
            {
                // Closes the parent form.
                this.Close();
            }
        }

        private void BtTest_Click(object sender, EventArgs e)
        {
            if (simpleSound != null)
            {
                simpleSound.Stop();
            }
            this.Hide();
            TestSettingsForm test = new TestSettingsForm();
            test.ShowDialog();
            this.Close();
        }
    }
}
