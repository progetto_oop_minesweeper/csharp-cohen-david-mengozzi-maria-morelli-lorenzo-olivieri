﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mengozziOop19
{
    class ReadWriteSettings : IReadWriteSettings
    {
        private string path;
        private readonly List<string> workLines;

        public ReadWriteSettings()
        {
            SetPath();
            string[] lines = File.ReadAllLines(path);
            workLines = lines.ToList<String>();

        }

        private void SetPath()
        {
            var homeDrive = Environment.GetEnvironmentVariable("HOMEDRIVE");
            if (!string.IsNullOrWhiteSpace(homeDrive))
            {

                var homePath = Environment.GetEnvironmentVariable("HOMEPATH");
                if (!string.IsNullOrWhiteSpace(homePath))
                {
                    var fullHomePath = homeDrive + Path.DirectorySeparatorChar + homePath;
                    path = Path.Combine(Path.Combine(fullHomePath, ".CSminesweeper"), "settings.txt");
                }
                else
                {
                    throw new Exception("Environment variable error, there is no 'HOMEPATH'");
                }
            }
            else
            {
                throw new Exception("Environment variable error, there is no 'HOMEDRIVE'");
            }
        }

        public string Mine
        {
            get
            {
                return workLines[0];
            }
            set
            {
                workLines[0] = value;
                Save();
            }
        }



        public string Color {
            get
            {
                return workLines[2];
            }
            set
            {
                workLines[2] = value;
                Save();
            }
        }

        public string Song {
            get
            {
                return workLines[3];
            }
            set
            {
                workLines[3] = value;
                Save();
            }
        }

        public string Flags {
            get
            {
                return workLines[1];
            }
            set
            {
                workLines[1] = value;
                Save();
            }
        }

        private void Save()
        {
            File.Delete(path);
            using (StreamWriter outputFile = new StreamWriter(path))
            {
                foreach (string line in workLines)
                    outputFile.WriteLine(line);
            }
        }
    }
}
