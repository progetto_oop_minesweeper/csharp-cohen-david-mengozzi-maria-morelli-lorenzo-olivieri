﻿using MengozziOOP19.Properties;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;

namespace mengozziOop19
{
    internal class LoadData : ILoadData
    {
        private string path;
        private readonly List<string> mineImage = new List<string>() { "panda.png", "bomba.png", "fiore.png", "panDiZenzero.png" };
        private readonly List<string> flagImage = new List<string>() { "bianca.png", "rossa.png", "scacchi.png", "puntina_verde.png" };
        private readonly List<string> sound = new List<string>() { "song01.wav", "song02.wav", "song03.wav", "song04.wav", "song05.wav",
                    "song06.wav", "song07.wav", "song08.wav", "song09.wav", "song10.wav" };
        /**controlla se esiste la cartella .CSminesweeper nella user home
         se esiste non fa nulla, altrimenti la crea con all'interno il file settings.txt contenuto in Resources*/
        public void Load()
        {
            
            var homeDrive = Environment.GetEnvironmentVariable("HOMEDRIVE");
            if (!string.IsNullOrWhiteSpace(homeDrive))
            {

                var homePath = Environment.GetEnvironmentVariable("HOMEPATH");
                if (!string.IsNullOrWhiteSpace(homePath))
                {
                    var fullHomePath = homeDrive + Path.DirectorySeparatorChar + homePath;
                    path = Path.Combine(fullHomePath, ".CSminesweeper");
                }
                else
                {
                    throw new Exception("Environment variable error, there is no 'HOMEPATH'");
                }
            }
            else
            {
                throw new Exception("Environment variable error, there is no 'HOMEDRIVE'");
            }
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
                File.WriteAllText(Path.Combine(path, "settings.txt"), Resources.settings);
                /**creo le cartelle che contengono le immagini e le canzoni */
                LoadMine();
                LoadFlag();
                LoadSong();
            }
        }

        private void LoadSong()
        {
            string songPath = Path.Combine(path, "Songs");
            Directory.CreateDirectory(songPath);
            foreach (string name in sound)
            {
                using (var fs = new FileStream(Path.Combine(songPath, name), FileMode.Create))
                {
                    Resources.ResourceManager.GetStream(Path.GetFileNameWithoutExtension(name)).CopyTo(fs);
                }
            }
        }

        private void LoadFlag()
        {
            string flagPath = Path.Combine(path, "FlagImg");
            Directory.CreateDirectory(flagPath);
            foreach (string name in flagImage)
            {
                Image img = (Image)Resources.ResourceManager.GetObject(Path.GetFileNameWithoutExtension(name));
                img.Save(Path.Combine(flagPath, name));

            }
        }

        private void LoadMine()
        {
            string minePath = Path.Combine(path, "MineImg");
            Directory.CreateDirectory(minePath);
            foreach(string name in mineImage)
            {
                Image img = (Image)Resources.ResourceManager.GetObject(Path.GetFileNameWithoutExtension(name));
                img.Save(Path.Combine(minePath, name));

            }
        }
    }
}