﻿namespace mengozziOop19
{
    partial class SettingsForm
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            this.title = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.pbSong = new System.Windows.Forms.PictureBox();
            this.toolStrip3 = new System.Windows.Forms.ToolStrip();
            this.tsSongSelector = new System.Windows.Forms.ToolStripSplitButton();
            this.stopButton = new System.Windows.Forms.Button();
            this.sound = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.pbFlag = new System.Windows.Forms.PictureBox();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.tsFlagSelector = new System.Windows.Forms.ToolStripSplitButton();
            this.btAddImgFlag = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.pbMine = new System.Windows.Forms.PictureBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsMineSelector = new System.Windows.Forms.ToolStripSplitButton();
            this.btAddImgMine = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.selectColor = new System.Windows.Forms.Button();
            this.exit = new System.Windows.Forms.Button();
            this.btTest = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbSong)).BeginInit();
            this.toolStrip3.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbFlag)).BeginInit();
            this.toolStrip2.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbMine)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // title
            // 
            this.title.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.title.AutoSize = true;
            this.title.Font = new System.Drawing.Font("Papyrus", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.title.ForeColor = System.Drawing.Color.Black;
            this.title.Location = new System.Drawing.Point(223, 9);
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(378, 76);
            this.title.TabIndex = 0;
            this.title.Text = "SETTINGS";
            this.title.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel3, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.sound, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel2, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.selectColor, 1, 2);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(4, 90);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(776, 324);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Controls.Add(this.pbSong);
            this.flowLayoutPanel3.Controls.Add(this.toolStrip3);
            this.flowLayoutPanel3.Controls.Add(this.stopButton);
            this.flowLayoutPanel3.Location = new System.Drawing.Point(391, 246);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(382, 75);
            this.flowLayoutPanel3.TabIndex = 5;
            // 
            // pbSong
            // 
            this.pbSong.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pbSong.Image = ((System.Drawing.Image)(resources.GetObject("pbSong.Image")));
            this.pbSong.Location = new System.Drawing.Point(3, 3);
            this.pbSong.Name = "pbSong";
            this.pbSong.Size = new System.Drawing.Size(100, 72);
            this.pbSong.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbSong.TabIndex = 0;
            this.pbSong.TabStop = false;
            // 
            // toolStrip3
            // 
            this.toolStrip3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.toolStrip3.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsSongSelector});
            this.toolStrip3.Location = new System.Drawing.Point(106, 26);
            this.toolStrip3.Name = "toolStrip3";
            this.toolStrip3.Size = new System.Drawing.Size(61, 25);
            this.toolStrip3.TabIndex = 1;
            this.toolStrip3.Text = "toolStrip3";
            // 
            // tsSongSelector
            // 
            this.tsSongSelector.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsSongSelector.Image = ((System.Drawing.Image)(resources.GetObject("tsSongSelector.Image")));
            this.tsSongSelector.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsSongSelector.Name = "tsSongSelector";
            this.tsSongSelector.Size = new System.Drawing.Size(49, 22);
            this.tsSongSelector.Text = "song";
            // 
            // stopButton
            // 
            this.stopButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.stopButton.Location = new System.Drawing.Point(170, 27);
            this.stopButton.Name = "stopButton";
            this.stopButton.Size = new System.Drawing.Size(75, 23);
            this.stopButton.TabIndex = 2;
            this.stopButton.Text = "■";
            this.stopButton.UseVisualStyleBackColor = true;
            this.stopButton.Click += new System.EventHandler(this.StopButton_Click);
            // 
            // sound
            // 
            this.sound.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.sound.AutoSize = true;
            this.sound.Font = new System.Drawing.Font("Segoe Print", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sound.Location = new System.Drawing.Point(145, 265);
            this.sound.Name = "sound";
            this.sound.Size = new System.Drawing.Size(97, 37);
            this.sound.TabIndex = 4;
            this.sound.Text = "SOUND";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe Print", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(147, 184);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 37);
            this.label3.TabIndex = 3;
            this.label3.Text = "COLOR";
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.pbFlag);
            this.flowLayoutPanel2.Controls.Add(this.toolStrip2);
            this.flowLayoutPanel2.Controls.Add(this.btAddImgFlag);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(391, 84);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(382, 75);
            this.flowLayoutPanel2.TabIndex = 3;
            // 
            // pbFlag
            // 
            this.pbFlag.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pbFlag.Location = new System.Drawing.Point(3, 3);
            this.pbFlag.Name = "pbFlag";
            this.pbFlag.Size = new System.Drawing.Size(100, 72);
            this.pbFlag.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbFlag.TabIndex = 0;
            this.pbFlag.TabStop = false;
            // 
            // toolStrip2
            // 
            this.toolStrip2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.toolStrip2.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsFlagSelector});
            this.toolStrip2.Location = new System.Drawing.Point(106, 26);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(55, 25);
            this.toolStrip2.TabIndex = 1;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // tsFlagSelector
            // 
            this.tsFlagSelector.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsFlagSelector.Image = ((System.Drawing.Image)(resources.GetObject("tsFlagSelector.Image")));
            this.tsFlagSelector.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsFlagSelector.Name = "tsFlagSelector";
            this.tsFlagSelector.Size = new System.Drawing.Size(43, 22);
            this.tsFlagSelector.Text = "flag";
            // 
            // btAddImgFlag
            // 
            this.btAddImgFlag.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btAddImgFlag.Location = new System.Drawing.Point(164, 27);
            this.btAddImgFlag.Name = "btAddImgFlag";
            this.btAddImgFlag.Size = new System.Drawing.Size(75, 23);
            this.btAddImgFlag.TabIndex = 3;
            this.btAddImgFlag.Text = "ADD IMG";
            this.btAddImgFlag.UseVisualStyleBackColor = true;
            this.btAddImgFlag.Click += new System.EventHandler(this.BtAddImgFlag_Click);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe Print", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(156, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 37);
            this.label1.TabIndex = 0;
            this.label1.Text = "MINE";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.pbMine);
            this.flowLayoutPanel1.Controls.Add(this.toolStrip1);
            this.flowLayoutPanel1.Controls.Add(this.btAddImgMine);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(391, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(382, 75);
            this.flowLayoutPanel1.TabIndex = 1;
            // 
            // pbMine
            // 
            this.pbMine.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pbMine.Location = new System.Drawing.Point(3, 3);
            this.pbMine.Name = "pbMine";
            this.pbMine.Size = new System.Drawing.Size(100, 72);
            this.pbMine.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbMine.TabIndex = 0;
            this.pbMine.TabStop = false;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsMineSelector});
            this.toolStrip1.Location = new System.Drawing.Point(106, 26);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(62, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tsMineSelector
            // 
            this.tsMineSelector.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsMineSelector.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsMineSelector.Name = "tsMineSelector";
            this.tsMineSelector.Size = new System.Drawing.Size(50, 22);
            this.tsMineSelector.Text = "mine";
            // 
            // btAddImgMine
            // 
            this.btAddImgMine.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btAddImgMine.Location = new System.Drawing.Point(171, 27);
            this.btAddImgMine.Name = "btAddImgMine";
            this.btAddImgMine.Size = new System.Drawing.Size(75, 23);
            this.btAddImgMine.TabIndex = 2;
            this.btAddImgMine.Text = "ADD IMG";
            this.btAddImgMine.UseVisualStyleBackColor = true;
            this.btAddImgMine.Click += new System.EventHandler(this.BtAddImgMine_Click);
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe Print", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(157, 103);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 37);
            this.label2.TabIndex = 2;
            this.label2.Text = "FLAG";
            // 
            // selectColor
            // 
            this.selectColor.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.selectColor.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.selectColor.Location = new System.Drawing.Point(532, 179);
            this.selectColor.Name = "selectColor";
            this.selectColor.Size = new System.Drawing.Size(100, 47);
            this.selectColor.TabIndex = 0;
            this.selectColor.Text = "COLOR SELECTOR";
            this.selectColor.UseVisualStyleBackColor = true;
            this.selectColor.Click += new System.EventHandler(this.SelectColor_Click);
            // 
            // exit
            // 
            this.exit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.exit.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exit.Location = new System.Drawing.Point(687, 420);
            this.exit.Name = "exit";
            this.exit.Size = new System.Drawing.Size(90, 29);
            this.exit.TabIndex = 2;
            this.exit.Text = "Exit";
            this.exit.UseVisualStyleBackColor = true;
            this.exit.Click += new System.EventHandler(this.ExitButton_Click);
            // 
            // btTest
            // 
            this.btTest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btTest.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btTest.Location = new System.Drawing.Point(12, 420);
            this.btTest.Name = "btTest";
            this.btTest.Size = new System.Drawing.Size(111, 29);
            this.btTest.TabIndex = 3;
            this.btTest.Text = "Test settings";
            this.btTest.UseVisualStyleBackColor = true;
            this.btTest.Click += new System.EventHandler(this.BtTest_Click);
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.ClientSize = new System.Drawing.Size(785, 458);
            this.Controls.Add(this.btTest);
            this.Controls.Add(this.exit);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.title);
            this.Name = "SettingsForm";
            this.Text = "Settings";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.flowLayoutPanel3.ResumeLayout(false);
            this.flowLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbSong)).EndInit();
            this.toolStrip3.ResumeLayout(false);
            this.toolStrip3.PerformLayout();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbFlag)).EndInit();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbMine)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label title;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.PictureBox pbMine;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripSplitButton tsMineSelector;
        private System.Windows.Forms.Button btAddImgMine;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.PictureBox pbFlag;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripSplitButton tsFlagSelector;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button selectColor;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.PictureBox pbSong;
        private System.Windows.Forms.ToolStrip toolStrip3;
        private System.Windows.Forms.ToolStripSplitButton tsSongSelector;
        private System.Windows.Forms.Label sound;
        private System.Windows.Forms.Button stopButton;
        private System.Windows.Forms.Button btAddImgFlag;
        private System.Windows.Forms.Button exit;
        private System.Windows.Forms.Button btTest;
    }
}

