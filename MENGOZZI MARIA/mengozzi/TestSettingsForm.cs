﻿using mengozziOop19;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MengozziOOP19
{
    public partial class TestSettingsForm : Form
    {
        private IReadWriteSettings rwSett;
        private string minePath;
        private string flagPath;
        private string songPath;
        private List<Boolean> btFlag = new List<bool>() { false, false, false, false };
        private SoundPlayer simpleSound;

        public TestSettingsForm()
        {
            InitializeComponent();
            rwSett = new ReadWriteSettings();
            LoadPath();
            AddColor();
            Button1.MouseDown += new MouseEventHandler(Button1_Click);
            Button2.MouseDown += new MouseEventHandler(Button2_Click);
            Button3.MouseDown += new MouseEventHandler(Button3_Click);
            Button4.MouseDown += new MouseEventHandler(Button4_Click);
        }

        private void AddColor()
        {
            string colorName = rwSett.Color;
            Color cl = Color.FromName(colorName);
            if (!cl.IsKnownColor)
            {
                string hexCol = string.Concat("#", colorName);
                cl = System.Drawing.ColorTranslator.FromHtml(hexCol);
            }
            title.ForeColor = cl;
            Button1.BackColor = cl;
            Button2.BackColor = cl;
            Button3.BackColor = cl;
            Button4.BackColor = cl;
            Music.BackColor = cl;
            BackSettings.BackColor = cl;
        }

        private void LoadPath()
        {
            var homeDrive = Environment.GetEnvironmentVariable("HOMEDRIVE");
            if (!string.IsNullOrWhiteSpace(homeDrive))
            {

                var homePath = Environment.GetEnvironmentVariable("HOMEPATH");
                if (!string.IsNullOrWhiteSpace(homePath))
                {
                    var fullHomePath = homeDrive + Path.DirectorySeparatorChar + homePath;
                    string path = Path.Combine(fullHomePath, ".CSminesweeper");
                    flagPath = Path.Combine(path, "FlagImg");
                    minePath = Path.Combine(path, "MineImg");
                    songPath = Path.Combine(path, "Songs");
                }
                else
                {
                    throw new Exception("Environment variable error, there is no 'HOMEPATH'");
                }
            }
            else
            {
                throw new Exception("Environment variable error, there is no 'HOMEDRIVE'");
            }
        }

        private void BackSettings_Click(object sender, EventArgs e)
        {
            if (simpleSound != null)
            {
                simpleSound.Stop();
            }
            this.Hide();
            SettingsForm sett = new SettingsForm();
            sett.ShowDialog();
            this.Close();
        }

        private void Button_Click(object sender, MouseEventArgs me, int num)
        {
            Button button = sender as Button;
            if (me.Button == MouseButtons.Left)
            {
                if (!this.btFlag[num])
                {
                    Image img = Image.FromFile(Path.Combine(flagPath, this.rwSett.Flags));
                    button.BackgroundImage = img;
                    button.BackgroundImageLayout = ImageLayout.Stretch;
                    btFlag[num] = true;
                }
                else
                {
                    button.BackgroundImage = null;
                    btFlag[num] = false;
                }

            }
            else
            {
                if (!this.btFlag[num])
                {
                    Image img = Image.FromFile(Path.Combine(minePath, this.rwSett.Mine));
                    button.BackgroundImage = img;
                    button.BackgroundImageLayout = ImageLayout.Stretch;
                    button.Enabled = false;
                } 
            }
        }

        private void Button1_Click(object sender, MouseEventArgs me)
        {
            this.Button_Click(sender, me, 0);
        }

        private void Button2_Click(object sender, MouseEventArgs me)
        {
            this.Button_Click(sender, me, 1);
        }

        private void Button3_Click(object sender, MouseEventArgs me)
        {
            this.Button_Click(sender, me, 2);
        }

        private void Button4_Click(object sender, MouseEventArgs me)
        {
            this.Button_Click(sender, me, 3);
        }

        private void Mute_Click(object sender, EventArgs e)
        {
            if (simpleSound != null)
            {
                simpleSound.Stop();
                simpleSound = null;
            } else
            {
                simpleSound = new SoundPlayer(Path.Combine(songPath, rwSett.Song));
                simpleSound.Play();
            }
        }
    }
}
