﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mengozziOop19
{
    interface IReadWriteSettings
    {
        string Mine { get; set; }
        string Flags { get; set; }
        string Color { get; set; }
        string Song { get; set; }
    }
}
